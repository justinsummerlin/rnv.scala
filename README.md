# rnv.scala

A port of RNV to readable Scala.

## About

[Relax NG](http://relaxng.org/) is a schema language for XML documents. It also has a variant language called RNC, or Relax NG Compact, abbreviated RNC.
One of the better RNC validators is called [RNV](http://www.davidashen.net/rnv.html), but it hasn't been actively developed or maintained and is written in C.
This project is a rewrite of RNV in Scala meant for use on the JVM or in node.js or web applications via Scala.js.